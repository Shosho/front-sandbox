import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {HomeComponent} from '../component/home/home.component';
import {PictureComponent} from '../component/picture/picture.component';
import {LadderComponent} from '../component/ladder/ladder.component';
import {LoginComponent} from '../component/user/login/login.component';
import {SignUpComponent} from '../component/user/sign-up/sign-up.component';

export const routes: Routes = [
  { path: '',  component: HomeComponent },
  { path: 'login',  component: LoginComponent },
  { path: 'sign-up',  component: SignUpComponent },
  { path: 'ladder', component: LadderComponent, data: { name: 'Ladder', snav: true } },
  { path: 'pictures', component: PictureComponent, data: { name: 'Pictures', snav: true } },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
