export interface Picture {
  id: number;
  name: string;
  path: string;
  description: string;
  subtitle?: string;
}
