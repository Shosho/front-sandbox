export interface Character {
  rank: number;
  dead: boolean;
  online: boolean;
  character: {
    name: string;
    level: number;
    class: string;
  };
  account: {
    name: string;
    twitch: {
      name: string;
    }
  };
}
