import {Character} from './character';

export interface CharacterResponse {
  total: number;
  entries: Character[];
}
