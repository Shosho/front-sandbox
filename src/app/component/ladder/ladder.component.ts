import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';

import {LadderService} from '../../service/ladder/ladder.service';
import {Character} from '../../interface/character';
import {LeagueService} from '../../service/league/league.service';
import {League} from '../../interface/league';
import {SnackbarService} from '../../service/snackbar/snackbar.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {tap} from 'rxjs/operators';
import {MatButtonToggleGroup} from '@angular/material/button-toggle';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-ladder',
  templateUrl: './ladder.component.html',
  styleUrls: ['./ladder.component.scss']
})
export class LadderComponent implements AfterViewInit, OnInit  {
  displayedColumns: string[] = ['rank', 'account', 'name', 'level', 'class', 'online', 'dead', 'twitch'];
  dataSource: MatTableDataSource<Character>;
  leagues: League[];
  total = 0;
  subscription: Subscription;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatButtonToggleGroup) button: MatButtonToggleGroup;

  constructor(
    private ladderService: LadderService,
    private leagueService: LeagueService,
    private snackbarService: SnackbarService
  ) { }

  ngOnInit(): void {
    this.leagueService.getLeagues().subscribe({
      next: value => {
        this.leagues = value;
        this.getLeagueCharacters(this.leagues[0].id);
      },
      error: err => this.snackbarService.error(err.error.error.message)
    });
  }


  getLeagueCharacters(value: string, offset = 0, limit = 100): void
  {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }

    this.subscription = this.ladderService.getLadder(value, offset, limit).subscribe({
      next: res => {
        this.total = res.total;
        this.dataSource = new MatTableDataSource<Character>(res.entries);

        if (this.paginator && offset === 0) {
          this.paginator.pageIndex = 0;
        }
      },
      error: err => this.snackbarService.error(err.error.error.message)
    });
  }

  ngAfterViewInit(): void {
    if (this.paginator) {
      this.paginator.page.pipe(tap(() => {
        this.getLeagueCharacters(this.button.value, this.paginator.pageIndex * this.paginator.pageSize);
      })).subscribe();
    }
  }
}
