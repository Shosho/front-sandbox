import {Component, OnInit} from '@angular/core';
import {PictureService} from '../../service/picture/picture.service';
import {Picture} from '../../interface/picture';
import {SnackbarService} from '../../service/snackbar/snackbar.service';

@Component({
  selector: 'app-picture',
  templateUrl: './picture.component.html',
  styleUrls: ['./picture.component.scss']
})
export class PictureComponent implements OnInit {
  public pictures: Picture[];
  title: string;
  subtitle?: string;
  description: string;
  file?: File;
  uploadedPath: string | ArrayBuffer;
  defaultPath = 'assets/upload_img.png';

  constructor(
    private pictureService: PictureService,
    private snackbarService: SnackbarService
  ) {

  }

  ngOnInit(): void {
    this.pictureService.getPictures().subscribe({
      next: pictures => this.pictures = pictures,
      error: () => this.snackbarService.error('Unknown error, please retry.')
    });
  }

  removePicture(pictureId: number): void {
    this.pictureService.removePicture(pictureId).subscribe({
      next: pictures => this.pictures = pictures,
      error: err => this.snackbarService.error(
        err.status === 404 ?
          'Picture was not found.' :
          'Unknown error, please retry.'
      ),
      complete: () => this.snackbarService.success('Picture was successfully deleted.')
    });
  }

  addPicture(): void {
    if (this.title === undefined ||
      this.title.length === 0
    ) {
      this.snackbarService.error('Please add a title before submitting');
      return;
    }

    if (this.description === undefined ||
      this.description.length === 0
    ) {
      this.snackbarService.error('Please add a description before submitting');
      return;
    }

    if (this.file === null) {
      this.snackbarService.error('Please add a file before submitting');
      return;
    }

    this.pictureService.addPicture(this.title, this.description, this.file, this.subtitle).subscribe({
      next: pictures => {
        this.pictures = pictures;
        this.title = '';
        this.subtitle = '';
        this.file = null;
        this.uploadedPath = this.defaultPath;
        this.description = '';
      },
      error: err => this.snackbarService.error(err.error.detail),
      complete: () => this.snackbarService.success('Picture was successfully added.')
    });
  }

  previewImage(files: FileList): void {
    if (files.length === 0) {
      this.uploadedPath = this.defaultPath;
      this.file = null;
      return;
    }

    if (files.item(0).type.match(/image\/*/) == null) {
      this.snackbarService.error('Only images are supported.');
      return;
    }

    const reader = new FileReader();
    this.file = files.item(0);
    reader.readAsDataURL(files.item(0));
    reader.onload = () => {
      this.uploadedPath = reader.result;
    };
  }
}
