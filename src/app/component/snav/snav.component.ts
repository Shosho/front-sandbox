import { Component } from '@angular/core';

import { routes } from '../../app-routing/app-routing.module';

@Component({
  selector: 'app-snav',
  templateUrl: './snav.component.html',
  styleUrls: ['./snav.component.scss']
})
export class SnavComponent {
  routes = routes;
}
