import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {LoginService} from '../../../service/user/login.service';
import {SnackbarService} from '../../../service/snackbar/snackbar.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private snackbarService: SnackbarService,
    private router: Router
  ) {

  }

  ngOnInit(): void
  {
    if (this.loginService.isAuthenticated()) {
      this.router.navigate(['']);
    }

    this.form = this.formBuilder.group({
      email: ['', Validators.email],
      password: ['', Validators.required]
    });
  }

  login(): void
  {
    if (this.form.valid) {
      this.loginService.login(this.form.get('email').value, this.form.get('password').value).subscribe({
        next: value => localStorage.setItem('user', JSON.stringify(value)),
        error: err => this.snackbarService.error(err.error.message),
        complete: () => {
          this.snackbarService.success('User successfly logged in.');
          location.reload();
        }
      });
    }
  }

  logout(): void
  {
    this.loginService.logout();
  }
}
