import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../../../service/user/login.service';
import {SnackbarService} from '../../../service/snackbar/snackbar.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private snackbarService: SnackbarService,
    private router: Router
  ) {

  }

  ngOnInit(): void {
    if (this.loginService.isAuthenticated()) {
      this.router.navigate(['']);
    }

    this.form = this.formBuilder.group({
      email: ['', Validators.email],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    });
  }

  signUp(): void
  {
    if (this.form.get('password').value !== this.form.get('confirmPassword').value) {
      this.snackbarService.error('Passwords must match.');
      return;
    }

    if (this.form.valid) {
      this.loginService.signUp(this.form.get('email').value, this.form.get('password').value).subscribe({
        error: err => this.snackbarService.error(err.error.detail),
        complete: () => {
          this.snackbarService.success('User successfly registered.');
          this.router.navigate(['/login']);
        }
      });
    }
  }
}
