import { Component } from '@angular/core';
import {LoginService} from './service/user/login.service';
import {User} from './interface/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  user: User;

  constructor(
    private loginService: LoginService
  ) {
    this.user = this.loginService.getUser();
  }

  logout(): void {
    this.loginService.logout();
    this.user = null;
  }
}
