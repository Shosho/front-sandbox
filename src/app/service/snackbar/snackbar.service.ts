import {Injectable, NgZone} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  constructor(
    public snackBar: MatSnackBar,
    private zone: NgZone
  ) {

  }

  private open(message: string, action = 'Dismiss', config: {}): void
  {
    this.zone.run(() => this.snackBar.open(message, action, config));
  }

  public success(message: string, action = 'Dismiss', duration = 2500): void
  {
    this.open(message, action, {duration, panelClass: 'success-snackbar'});
  }

  public error(message: string, action = 'Dismiss', duration = 2500): void
  {
    this.open(message, action, {duration, panelClass: 'warn-snackbar'});
  }
}
