import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import {API_URL} from '../../../environments/environment';
import {User} from '../../interface/user';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(
    private httpClient: HttpClient
  ) {

  }


  login(email: string, password: string): Observable<User> {
    return this.httpClient.post<User>(
      `${API_URL}login`,
      {email, password}
    );
  }

  getUser(): User | null
  {
    return JSON.parse(localStorage.getItem('user'));
  }

  isAuthenticated(): boolean
  {
    return this.getUser() !== null;
  }

  logout(): void
  {
    localStorage.removeItem('user');
  }

  signUp(email: string, password: string): Observable<any>
  {
    return this.httpClient.post(
      `${API_URL}sign-up`,
      {email, password}
    );
  }
}
