import { TestBed } from '@angular/core/testing';

import { LadderService } from './ladder.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('LadderService', () => {
  let service: LadderService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
    });
    service = TestBed.inject(LadderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
