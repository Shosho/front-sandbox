import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import {POE_API_URL} from '../../../environments/environment';
import {CharacterResponse} from '../../interface/character-response';

@Injectable({
  providedIn: 'root'
})
export class LadderService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getLadder(league: string, offset = 0, limit = 100): Observable<CharacterResponse>
  {
    return this.httpClient.get<CharacterResponse>(`${POE_API_URL}ladders/${league}?offset=${offset}&limit=${limit}`);
  }
}
