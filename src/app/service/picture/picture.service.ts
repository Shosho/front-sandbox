import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import { API_URL } from '../../../environments/environment';
import {Picture} from '../../interface/picture';

@Injectable({
  providedIn: 'root'
})
export class PictureService {
  constructor(private httpClient: HttpClient) {

  }

  public getPictures(): Observable<Picture[]> {
    return this.httpClient.get<Picture[]>(`${API_URL}pictures`);
  }

  public removePicture(pictureId: number): Observable<any> {
    return this.httpClient.delete(`${API_URL}pictures/${pictureId}`);
  }

  public addPicture(title: string, description: string, file: File, subtitle?: string): Observable<any>
  {
    const formData = new FormData();
    formData.append('title', title);
    formData.append('description', description);
    formData.append('file', file, file.name);


    if (subtitle !== undefined &&
      subtitle !== null
    ) {
      formData.append('subtitle', subtitle);
    }

    return this.httpClient.post(`${API_URL}pictures`, formData);
  }
}
