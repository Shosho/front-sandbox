import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import {League} from '../../interface/league';
import {POE_API_URL} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LeagueService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getLeagues(): Observable<League[]>
  {
    return this.httpClient.get<League[]>(`${POE_API_URL}leagues`);
  }
}
